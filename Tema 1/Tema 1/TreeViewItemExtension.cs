﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.Security.AccessControl;
using System.Security.Principal;

namespace Tema_1
{
    public static class TreeViewItemExtension
    {
        public static void ChangeColor(this TreeViewItem item, Brush color)
        {
            ((item.Header as Grid).Children[2] as TextBlock).Foreground = color;
        }

        public static TreeViewItem Find(this TreeViewItem treeItem, string directory)
        {
            foreach (TreeViewItem item in treeItem.Items)
            {
                Grid grid = item.Header as Grid;
                if ((grid.Children[0] as TextBlock).Text.Equals(directory))
                    return item;

                TreeViewItem tmpItem = item.Find(directory);
                if (tmpItem != null)
                    return tmpItem;
            }
            return null;
        }
    }
}
