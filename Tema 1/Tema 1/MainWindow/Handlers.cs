﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
namespace Tema_1
{
    public partial class MainWindow : Window
    {
        private void HandleBack(ref TreeView tree)
        {
            ref string parent = ref ParentSelector(tree);
            if (!tree.HasItems)
            {
                parent = tree.Back(parent, color.GetTextColor());
                return;
            }

            DiscElement director = new DiscElement(tree.Items.GetItemAt(0) as TreeViewItem);
            string text = director.GetText();

            string tmpParent = tree.Back(Operations.GetBackDirectory(text), color.GetTextColor());
            parent = tmpParent;

            if (parent == null)
                BackStatus(tree, false);
        }

        private void HandleBtnCopy()
        {
            string destination;
            DiscElement element;
            switch (Methods.ItemSelector(treeExplorer1, treeExplorer2))
            {
                default: return;
                case 1: element = new DiscElement(treeExplorer1.SelectedItem as TreeViewItem); destination = parent2; break;
                case 2: element = new DiscElement(treeExplorer2.SelectedItem as TreeViewItem); destination = parent1; break;
            }

            string item = element.GetText();

            if (destination == null)
                destination = "";

            destination = System.IO.Path.Combine(destination, Methods.GetFileName(item));

            if (!Operations.OperationCheck(item, destination))
                return;

            Methods.CreateDocument(item).Copy(destination);

            HandleRefresh();
        }

        private void HandleBtnMove()
        {
            string destination;
            DiscElement element;
            switch (Methods.ItemSelector(treeExplorer1, treeExplorer2))
            {
                default: return;
                case 1: element = new DiscElement(treeExplorer1.SelectedItem as TreeViewItem); destination = parent2; break;
                case 2: element = new DiscElement(treeExplorer2.SelectedItem as TreeViewItem); destination = parent1; break;
            }

            string item = element.GetText();

            if (destination == null)
                destination = "";

            destination = System.IO.Path.Combine(destination, Methods.GetFileName(item));

            if (!Operations.OperationCheck(item, destination))
                return;

            Methods.CreateDocument(item).Move(destination);

            HandleRefresh();
        }

        private void HandleDelete(string source)
        {
            if (!Directory.Exists(source))
            {
                Methods.DeleteFile(source);
                return;
            }

            if (!Methods.IsDirectoryEmpty(source))
                if (!DeleteDialog())
                    return;

            Methods.DeleteDirectory(source);
        }


        private void HandlePack()
        {
            string parent;
            DiscElement element;
            switch (Methods.ItemSelector(treeExplorer1, treeExplorer2))
            {
                default: return;
                case 1: element = new DiscElement(treeExplorer1.SelectedItem as TreeViewItem); parent = parent2; break;
                case 2: element = new DiscElement(treeExplorer2.SelectedItem as TreeViewItem); parent = parent1; break;
            }
            Methods.CreateDocument(element.GetText()).Pack(parent);
            HandleRefresh();
        }

        private void HandleUnpack()
        {
            string parent;
            DiscElement element;
            switch (Methods.ItemSelector(treeExplorer1, treeExplorer2))
            {
                default: return;
                case 1: element = new DiscElement(treeExplorer1.SelectedItem as TreeViewItem); parent = parent2; break;
                case 2: element = new DiscElement(treeExplorer2.SelectedItem as TreeViewItem); parent = parent1; break;
            }
            Methods.CreateDocument(element.GetText()).Unpack(parent);
            HandleRefresh();
        }

        private void HandleNewFolder()
        {
            string source;
            Side direction = Side.Both;

            string name = Microsoft.VisualBasic.Interaction.InputBox("Introduceti numele folderului ", "Nume Folder ", "Director nou");

            if (name == "")
                return;

            string side = Microsoft.VisualBasic.Interaction.InputBox("Specificati pe ce parte se doreste crearea folderului (Stanga sau Dreapta) ", "Alegeti partea", "Stanga");

            switch (side)
            {
                default: Message.ErrorMessage("Folderul nu a fost creat\n" + "Motiv: Nu a fost specificata o parte valida!"); return;
                case "Stanga": source = parent1; direction = Side.Left; break;
                case "Dreapta": source = parent2; direction = Side.Right; break;
                case "": return;
            }

            if (!Operations.OperationCheck("", source + "\\" + name))
                return;

            Directory.CreateDirectory(source + "\\" + name);

            HandleRefresh(direction);
        }

        private void HandleShortcut(object sender, KeyEventArgs e)
        {
            TreeView tree = sender as TreeView;
            switch (e.Key)
            {
                case Key.C: if (Methods.CtrlPressed()) HandleBtnCopy(); break;

                case Key.M: if (Methods.CtrlPressed()) HandleBtnMove(); break;

                case Key.Delete: BtnDelete_Click(null, null); break;

                case Key.N: if (Methods.CtrlPressed()) BtnNewFolder_Click(null, null); break;

                case Key.Back:
                    {
                        if (tree.Equals(treeExplorer1))
                            BtnBack1_Click(null, null);
                        else
                            BtnBack2_Click(null, null);
                        break;
                    }

                case Key.F5:
                    {
                        if (tree.Equals(treeExplorer1))
                            BtnRefresh1_Click(null, null);
                        else
                            BtnRefresh2_Click(null, null);
                        break;
                    }

                case Key.Enter:
                    {
                        if (tree.Equals(treeExplorer1))
                            TreeExplorer_Expanded(treeExplorer1, new RoutedEventArgs(null, treeExplorer1.SelectedItem));
                        else
                            TreeExplorer_Expanded(treeExplorer2, new RoutedEventArgs(null, treeExplorer2.SelectedItem));
                        break;
                    }
            }
        }

    }
}
