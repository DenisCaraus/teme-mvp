﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
namespace Tema_1
{
    public partial class MainWindow : Window
    {
        private void BackStatus(TreeView tree, bool status)
        {
            if (subMenuTree.IsChecked)
                status = false;
            if (tree.Equals(treeExplorer1))
                btnBack1.ChangeStatus(status);
            if (tree.Equals(treeExplorer2))
                btnBack2.ChangeStatus(status);
        }  

        private bool DeleteDialog()
        {
            MessageBoxResult result = Message.DeleteDirectoryMessage();

            switch (result)
            {
                default: return false;
                case MessageBoxResult.Yes: return true;
            }
        }

        private void ButtonStatus(bool status)
        {
            btnDelete.ChangeStatus(status);
            btnMove.ChangeStatus(status);
            btnCopy.ChangeStatus(status);
        }

        private void BackImageChange(Button back, Image image)
        {
            if (back.IsHitTestVisible == false)
            {
                image.Source = new BitmapImage(new Uri("../Icons/BackOff.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                image.Source = new BitmapImage(new Uri("../Icons/BackOn.png", UriKind.RelativeOrAbsolute));
            }
        }
    }
}
