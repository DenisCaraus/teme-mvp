﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.IO.Compression;

namespace Tema_1
{

    public partial class MainWindow : Window
    {
        enum Side { Both, Left, Right }

        private TreeView CloneLastTree()
        {
            TreeView tree = new TreeView() { Background = treeExplorer1.Background };
            if (selected1 == null)
            {
                tree.Generate(parent1, color.GetTextColor());
                parent3 = parent1;
            }
            else
            {
                tree.Generate(parent2, color.GetTextColor());
                parent3 = parent2;
            }
            tree.AddHandler(TreeViewItem.ExpandedEvent, (RoutedEventHandler)TreeExplorer_Expanded);

            return tree;
        }

        private Button CloneBackButton(TreeView tree)
        {
            Image image = new Image() { Source = new BitmapImage(new Uri("Icons/BackOn.png", UriKind.RelativeOrAbsolute)) };

            Button back = new Button() { Content = image, Height = 30, Width = 40, Margin = new Thickness(5, 0, 0, 0), HorizontalAlignment = HorizontalAlignment.Left, Background = btnBack1.Background, BorderThickness = new Thickness(0) };
            back.Click += (o, e) =>
            {
                HandleBack(ref tree);
            };

            return back;
        }

        private Button CloneRefreshButton(TreeView tree)
        {
            Image image = new Image() { Source = new BitmapImage(new Uri("Icons/Refresh.png", UriKind.RelativeOrAbsolute)) };
            Button refresh = new Button() { Content = image, Height = 30, Width = 30, Margin = new Thickness(0,0,0,0), BorderThickness = new Thickness(0), HorizontalAlignment = HorizontalAlignment.Left, Background = btnRefresh1.Background};
            refresh.Click += (o, e) =>
            {
                if (((tree.SelectedItem as TreeViewItem) != null) && (subMenuFull.IsChecked))
                    (tree.SelectedItem as TreeViewItem).Items.Clear();

                tree.FillTree(parent3, color.GetTextColor(), subMenuFull.IsChecked);
            };

            return refresh;
        }

        private Grid CreateExplorer()
        {
            TreeView tree = CloneLastTree();
            Grid grid = new Grid();
            grid.CreateTabGrid(CloneRefreshButton(tree), CloneBackButton(tree), tree);
            return grid;
        }

        private void MakeVertical()
        {
            RotateTransform rotate = new RotateTransform(90);
            grdMain.LayoutTransform = rotate;
            rotate = new RotateTransform(270);
            RotateTab(rotate);
        }

        private void MakeHorizontal()
        {
            RotateTransform rotate = new RotateTransform(0);
            grdMain.LayoutTransform = rotate;
            rotate = new RotateTransform(0);
            RotateTab(rotate);
        }

        private void ChangeOrientation()
        {
            if (subMenuVertical.IsChecked == true)
                MakeVertical();
            else
                MakeHorizontal();
        }

        private void RotateTab(RotateTransform rotate)
        {
            treeExplorer1.LayoutTransform = rotate;
            treeExplorer2.LayoutTransform = rotate;
            btnBack1.LayoutTransform = rotate;
            btnBack2.LayoutTransform = rotate;
            btnCopy.LayoutTransform = rotate;
            btnDelete.LayoutTransform = rotate;
            btnMove.LayoutTransform = rotate;
            btnNewFolder.LayoutTransform = rotate;
            btnRefresh1.LayoutTransform = rotate;
            btnRefresh2.LayoutTransform = rotate;
        }
    }
}
