﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.IO.Compression;

namespace Tema_1
{
    public partial class MainWindow : Window
    {
        private void HandleRefresh(Side direction = Side.Both)
        {
            if (subMenuTree.IsChecked)
            {
                if (((treeExplorer1.SelectedItem as TreeViewItem) != null) && (subMenuFull.IsChecked))
                    (treeExplorer1.SelectedItem as TreeViewItem).Items.Clear();
            }
            switch (direction)
            {
                case Side.Left: treeExplorer1.FillTree(parent1, color.GetTextColor(), subMenuFull.IsChecked); break;
                case Side.Right: treeExplorer2.FillTree(parent2, color.GetTextColor(), subMenuFull.IsChecked); break;
                default: treeExplorer1.FillTree(parent1, color.GetTextColor(), subMenuFull.IsChecked); treeExplorer2.FillTree(parent2, color.GetTextColor(), subMenuFull.IsChecked); break;
            }
        }

        private void Reset()
        {
            BackStatus(treeExplorer1, false);
            BackStatus(treeExplorer2, false);
            treeExplorer1.PutDrivesInTree(color.GetTextColor(), subMenuFull.IsChecked);
            treeExplorer2.PutDrivesInTree(color.GetTextColor(), subMenuFull.IsChecked);
        }
    }
}
