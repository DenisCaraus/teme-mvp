﻿using System;
using System.IO;
using System.Security.AccessControl;

namespace Tema_1
{
    class RightsControl
    {
        public static bool CanAccess(string file)
        {
            if (Directory.Exists(file))
                return CanAccessDir(file);
            else
                return CanAccessFile(file);
        }

        private static bool CanAccessDir(string directory)
        {
            DirectorySecurity accessControl;
            // in caz ca nu putem lua drepturile de access
            try
            {
                accessControl = Directory.GetAccessControl(directory);
            }
            catch(Exception)
            {
                return false;
            }

            if (accessControl == null)
                return false;

            return AccessCheck(accessControl.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier)));
            }

        private static bool CanAccessFile(string file)
        {
            FileSecurity accessControl;
            // in caz ca nu putem lua drepturile de access
            try
            {
                accessControl = File.GetAccessControl(file);
            }
            catch (Exception)
            {
                return false;
            }

            if (accessControl == null)
                return false;

            return AccessCheck(accessControl.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier)));
        }

        private static bool AccessCheck(AuthorizationRuleCollection rights)
        {
            bool allow = false,
                deny = false;
            if (rights == null)
                return false;

            foreach (FileSystemAccessRule rule in rights)
            {
                if ((FileSystemRights.Read & rule.FileSystemRights) != FileSystemRights.Read)
                    continue;

                if (rule.AccessControlType == AccessControlType.Allow)
                    allow = true;
                if (rule.AccessControlType == AccessControlType.Deny)
                    deny = true;
            }

            return allow && !deny;
        }
    }
}
