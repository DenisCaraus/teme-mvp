﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.Security.AccessControl;
using System.Security.Principal;
using System.IO.Compression;

namespace Tema_1
{
    public class Methods
    {
        public static void NotImplementedMessageBox(string name = "Functia")
        {
            MessageBox.Show(name + " nu este implementata in acest moment.\n",
                "Avertisment", MessageBoxButton.OK);
        }

        public static bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

        public static string GetFileName(string file)
        {
            if (file == null)
                return "";

            if (Directory.GetLogicalDrives().Contains(file))
                return file;
            else
            {
                file = file.Split('\\').Last();
                return file;
            }
        }

        public static string GetFileType(string file)
        {
            if (Directory.GetLogicalDrives().Contains(file))
                return "Drive";

            if (Directory.Exists(file))
                return "Folder";

            if (File.Exists(file))
                return "File";

            return "null";
        }

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        //  Because the Bitmap type cannot be used as a source for an Windows.Controls.Image type
        // we need to convert an Bitmap object to an BitmapSource object.
        public static BitmapSource BitmapSourceConverter(System.Drawing.Bitmap bitmap)
        {
            IntPtr hBitmap = bitmap.GetHbitmap();
            BitmapSource bmpImage;
            try
            {
                bmpImage = Imaging.CreateBitmapSourceFromHBitmap(
                             hBitmap,
                             IntPtr.Zero,
                             Int32Rect.Empty,
                             BitmapSizeOptions.FromEmptyOptions());
            }
            finally
            {
                // Hbitmaps must be deleted at the end of their use
                DeleteObject(hBitmap);
            }

            return bmpImage;
        }

        public static Image RetrieveFileIcon(string file)
        {
            System.Drawing.Icon icon = System.Drawing.Icon.ExtractAssociatedIcon(file);
            System.Drawing.Bitmap bitmap = icon.ToBitmap();
            BitmapSource bmp = BitmapSourceConverter(bitmap);

            return new Image()
            {
                Source = bmp,
                Height = 15,
                Width = 15
            };
        }

        public static string SizeUnitSelector(int selector)
        {
            switch (selector)
            {
                default: return "";
                case 0: return "B";
                case 1: return "KiB";
                case 2: return "MiB";
                case 3: return "GiB";
                case 4: return "TiB";
                case 5: return "PiB";
                case 6: return "EiB";
                case 7: return "ZiB";
                case 8: return "YiB";
            }
        }

        public static string GetFileLength(string path)
        {
            long size = new FileInfo(path).Length;
            int count = 0;

            while (size > 1024)
            {
                size /= 1024;
                count++;
            }

            return size.ToString() + " " + SizeUnitSelector(count);

        }

        public static bool IsDirectory(String directory)
        {
            if (directory == null)
                return false;

            if (!Directory.Exists(directory))
                return false;

            return true;
        }

        public static void HandleFile(string file)
        {
            if (!File.Exists(file))
                return;

            Process.Start(file);
        }

        public static UInt16 ItemSelector(TreeView tree1, TreeView tree2)
        {
            if ((tree1.SelectedItem != null) && (tree2.SelectedItem != null))
                return 0;
            if ((tree1.SelectedItem == null) && (tree2.SelectedItem == null))
                return 0;
            if (tree1.SelectedItem != null)
                return 1;
            else
                return 2;
        }

        public static void DeleteFile(string path)
        {
            System.IO.File.Delete(path);
        }

        public static void DeleteDirectory(string path)
        {
            System.IO.Directory.Delete(path, true);
        }

        public static DirectorySecurity EveryoneDirectoryAccessRule()
        {
            DirectorySecurity rule = new DirectorySecurity();
            rule.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                FileSystemRights.FullControl,
                AccessControlType.Allow));
            return rule;
        }

        public static bool CtrlPressed()
        {
            return Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
        }

        public static string[] LoadFile(string file)
        {
            if ((!Directory.Exists("Save")) || (!File.Exists(file)))
            {
                Save();
            }
            return File.ReadAllLines(file);
        }

        public static void Save(string value = "light")
        {
            string file = "Save\\SavedSettings.txt";

            if (!Directory.Exists("Save"))
            {
                Directory.CreateDirectory("Save");
            }

            File.WriteAllText(file, "theme=" + value + "\n");
        }

        //
        public static string CreateDirectoryName(string dir)
        {
            dir = dir.Split('.').First();
            int count = 0;
            while (Directory.Exists(dir))
            {
                count++;
                dir = dir + count.ToString();
            }
            return dir;
        }

        public static DocumentX CreateDocument(string item)
        {
            DocumentX document = new DocumentX(item);

            switch (document.GetType())
            {
                case "Folder": document = new FolderX(item); break;
                case "File": document = new FileX(item); break;
                default: break;
            }

            return document;
        }
    }
}
