﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace Tema_1
{
    public class DocumentX
    {
        protected string documentPath;

        public DocumentX(string documentPath)
        {
            this.documentPath = documentPath;
        }

        public string GetDocumentPath()
        {
            return documentPath;
        }

        public new string GetType()
        {
            if (Directory.Exists(documentPath))
            {
                return "Folder";
            }
            if (File.Exists(documentPath))
            {
                return "File";
            }
            return null;
        }


        public virtual bool Copy(string destination)
        {
            throw new NotImplementedException("You cannot use the base class for this operation!");
        }

        public virtual bool Move(string destination)
        {
            throw new NotImplementedException("You cannot use the base class for this operation!");
        }

        public virtual bool Pack(string destination)
        {
            throw new NotImplementedException("You cannot use the base class for this operation!");
        }

        public bool Unpack(string destination)
        {
            if (System.IO.Directory.GetLogicalDrives().Contains(documentPath)) { return false; }
            if (System.IO.Directory.GetLogicalDrives().Contains(destination)) { return false; }
            if (destination == null) { return false; }

            ZipFile.ExtractToDirectory(documentPath, destination);

            return true;
        }

        public string CreateDirectoryName(string dir)
        {
            dir = dir.Split('.').First();
            int count = 0;
            while (Directory.Exists(dir))
            {
                count++;
                dir = dir + count.ToString();
            }
            return dir;
        }

    }
}
