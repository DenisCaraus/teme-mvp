﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_1
{
    public class FileX : DocumentX
    {

        public FileX(string fileName) : base(fileName)
        {
            // empty
        }

        public string FileName
        {
            get
            {
                return documentPath;
            }
            set
            {
                documentPath = value;
            }
        }

        private bool Check(string destination)
        {
            if (destination == null) { return false; }
            if (!File.Exists(documentPath)) { return false; }
            return true;
        }

        public override bool Copy(string destination)
        {
            if (!Check(destination)) { return false; }

            File.Copy(documentPath, destination, true);

            return true;
        }

        public override bool Move(string destination)
        {
            if (!Check(destination)) { return false; }

            File.Move(documentPath, destination);

            return true;
        }

        public override bool Pack(string destination)
        {
            if (!Check(destination)) { return false; }
            destination = Path.Combine(destination, Methods.GetFileName(documentPath).Split('.').First() + ".zip");
            if (File.Exists(destination)) { return false; }

            string newItem = CreateDirectoryName(documentPath);
            Directory.CreateDirectory(newItem);

            Methods.CreateDocument(documentPath).Copy(Path.Combine(newItem, Methods.GetFileName(documentPath)));
            
            ZipFile.CreateFromDirectory(newItem, destination);

            Methods.DeleteDirectory(newItem);

            return true;
        }
    }
}
