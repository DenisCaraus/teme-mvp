﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace Tema_1
{
    public class FolderX : DocumentX
    {

        public FolderX(string folderName) : base(folderName)
        {
            // empty
        }

        public string FolderName
        {
            get
            {
                return documentPath;
            }
            set
            {
                documentPath = value;
            }
        }

        private bool Check(string destination)
        {
            if (destination == null) { return false; }
            if (!Directory.Exists(documentPath)) { return false; }
            return true;
        }

        public override bool Copy(string destination)
        {
            if (!Check(destination)) { return false; }

            new Microsoft.VisualBasic.Devices.Computer().FileSystem.CopyDirectory(documentPath, destination);

            return true;
        }

        public override bool Move(string destination)
        {
            if (!Check(destination)) { return false; }

            Directory.Move(documentPath, destination);

            return true;
        }

        public override bool Pack(string destination)
        {
            if (!Check(destination)) { return false; }

            destination = System.IO.Path.Combine(destination, Methods.GetFileName(documentPath));
            ZipFile.CreateFromDirectory(documentPath, destination);

            return true;
        }
    }
}
