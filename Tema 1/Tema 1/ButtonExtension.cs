﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.Security.AccessControl;
using System.Security.Principal;

namespace Tema_1
{
    public static class ButtonExtension
    {
        public static void ChangeStatus(this Button button, bool status)
        {
            if (status)
                button.Enable();
            else
                button.Disable();
        }

        private static void Disable(this Button button)
        {
            button.IsHitTestVisible = false;
        }

        private static void Enable(this Button button)
        {
            button.IsHitTestVisible = true;
        }
    }
}
