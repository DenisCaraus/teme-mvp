﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.IO.Compression;

namespace Tema_1
{
    class Operations
    {
        public static bool OperationCheck(string source, string destination)
        {
            if ((destination == null) || (source == null))
            {
                Message.ErrorMessage(" Nu puteti efectua aceasta operatie! \n Motiv: Nu se pot efectua operatii pe ecranul de alegere a partitiilor!");
                return false;
            }
            if (source.Equals(destination))
            {
                Message.ErrorMessage(" Nu puteti efectua aceasta operatie! \n Motiv: Sursa si destinatia sunt identice!");
                return false;
            }
            if (System.IO.Directory.GetLogicalDrives().Contains(destination) || (destination[0] == '\\') || System.IO.Directory.GetLogicalDrives().Contains(source))
            {
                Message.ErrorMessage(" Nu puteti efectua aceasta operatie! \n Motiv: Nu se pot efectua operatii pe ecranul de alegere a partitiilor!");
                return false;
            }
            if (System.IO.Directory.Exists(destination) || System.IO.File.Exists(destination))
            {
                Message.ErrorMessage(" Nu puteti efectua aceasta operatie! \n Motiv: Exista deja un fisier cu acelasi nume la destinatie");
                return false;
            }
            return true;
        }

        public static string GetBackDirectory(string directory)
        {
            System.IO.DirectoryInfo parent = Directory.GetParent(directory);

            if (parent == null)
            {
                Message.ErrorMessage("Nu mai puteti merge inapoi!");
                return null;
            }

            return parent.ToString();
        }

    }
}
