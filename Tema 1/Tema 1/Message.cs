﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.Security.AccessControl;
using System.Security.Principal;
using System.IO.Compression;

namespace Tema_1
{
    class Message
    {
        public static void NotImplementedMessageBox(string name = "Functia")
        {
            MessageBox.Show(name + " nu este implementata in acest moment.\n",
                "Avertisment", MessageBoxButton.OK);
        }

        public static void ErrorMessage(string error)
        {
            MessageBox.Show(error,
                "Avertisment", MessageBoxButton.OK);
        }

        public static MessageBoxResult DeleteDirectoryMessage()
        {
            return MessageBox.Show("Directorul selectat nu este gol!\n" +
                "Doriti totusi sa-l stergeti?",
                "Avertisment", MessageBoxButton.YesNo);
        }

        public static MessageBoxResult RestartApplication()
        {
            return MessageBox.Show("Pentru a aplica setarile trebuie sa reporniti aplicatia!\n" +
                "Reporniti?",
                "Avertisment", MessageBoxButton.YesNo);
        }
    }
}
