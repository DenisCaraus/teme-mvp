﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.Security.AccessControl;
using System.Security.Principal;

namespace Tema_1
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string parent1, parent2, parent3;
        private TreeViewItem selected1, selected2;
        private UInt16 lastTree = 0;
        private ElementColor color;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Load();
            BackStatus(treeExplorer1, false);
            BackStatus(treeExplorer2, false);
            subMenuFull.IsChecked = true;
            ButtonStatus(false);
            Reset();
        }

        private void Load()
        {
            string[] optionList = Methods.LoadFile("Save\\SavedSettings.txt");
            string[] value = optionList[0].Split('=');
            value[1] = value[1].ToLower();

            switch (value[1])
            {
                default: SetTheme(ElementColor.Theme.Light); break;
                case "light": SetTheme(ElementColor.Theme.Light); break;
                case "dark": SetTheme(ElementColor.Theme.Dark); break;
            }
        }

        private void ResetSelected()
        {
            if (selected1 != null)
            {
                selected1.ChangeColor(color.GetTextColor());
                selected1 = null;
            }
            if (selected2 != null)
            {
                selected2.ChangeColor(color.GetTextColor());
                selected2 = null;
            }
        }

        private void ResetChecked(MenuItem item)
        {
            if (item == subMenuFull)
                subMenuTree.IsChecked = false;
            if (item == subMenuTree)
                subMenuFull.IsChecked = false;
            if (item != subMenuVertical)
                subMenuVertical.IsChecked = false;
            if (item != subMenuTab)
                subMenuTab.IsChecked = false;
        }

        private ref string ParentSelector(TreeView tree)
        {
            if (tree.Equals(treeExplorer1))
                return ref parent1;
            else if (tree.Equals(treeExplorer2))
                return ref parent2;
            else
                return ref parent3;
        }

        private void CreateExplorerTab()
        {
            tabMain.Height = 20;
            tabExtra.Visibility = Visibility.Visible;
            tabExtra.Content = CreateExplorer();
            tabExtra.Focus();
        }

        private void BtnBack1_Click(object sender, RoutedEventArgs e)
        {
            HandleBack(ref treeExplorer1);
            ButtonStatus(false);
        }

        private void BtnBack2_Click(object sender, RoutedEventArgs e)
        {
            HandleBack(ref treeExplorer2);
            ButtonStatus(false);
        }

        private void TreeExplorer_Expanded(object sender, RoutedEventArgs e)
        {
            TreeView tree = sender as TreeView;
            DiscElement element = new DiscElement(e.OriginalSource as TreeViewItem);
            string text = element.GetText();

            if (!Methods.IsDirectory(text))
            {
                Methods.HandleFile(text);
                return;
            }

            if (!tree.FillTree(text, color.GetTextColor(), subMenuFull.IsChecked))
                return;

            ref string parent = ref ParentSelector(tree);
            parent = text;

            bool status = true;
            if (parent == null)
                status = false;
            BackStatus(tree, status);

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            string source;
            DiscElement element;
            switch (Methods.ItemSelector(treeExplorer1, treeExplorer2))
            {
                default: return;
                case 1: element = new DiscElement(treeExplorer1.SelectedItem as TreeViewItem); break;
                case 2: element = new DiscElement(treeExplorer2.SelectedItem as TreeViewItem); break;
            }

            source = element.GetText();

            HandleDelete(source);
            HandleRefresh();
        }

        private void TreeExplorer1_Selected(object sender, RoutedEventArgs e)
        {
            lastTree = 1;
            if (treeExplorer2.SelectedItem != null)
            {
                ResetSelected();
                selected2 = treeExplorer2.SelectedItem as TreeViewItem;
                selected2.ChangeColor(Brushes.Red);
                (treeExplorer2.SelectedItem as TreeViewItem).IsSelected = false;
            }
            ButtonStatus(true);
        }

        private void TreeExplorer1_Unselected(object sender, RoutedEventArgs e)
        {
            if (treeExplorer2.SelectedItem == null)
                ButtonStatus(false);
            else
                ButtonStatus(true);
        }

        private void TreeExplorer2_Selected(object sender, RoutedEventArgs e)
        {
            if (treeExplorer1.SelectedItem != null)
            {
                ResetSelected();
                selected1 = treeExplorer1.SelectedItem as TreeViewItem;
                selected1.ChangeColor(Brushes.Red);
                (treeExplorer1.SelectedItem as TreeViewItem).IsSelected = false;
            }
            ButtonStatus(true);
        }

        private void TreeExplorer2_Unselected(object sender, RoutedEventArgs e)
        {
            if (treeExplorer1.SelectedItem == null)
                ButtonStatus(false);
            else
                ButtonStatus(true);
        }

        private void BtnRefresh1_Click(object sender, RoutedEventArgs e)
        {
            HandleRefresh(Side.Left);
        }

        private void BtnRefresh2_Click(object sender, RoutedEventArgs e)
        {
            HandleRefresh(Side.Right);
        }

        private void BtnNewFolder_Click(object sender, RoutedEventArgs e)
        {
            HandleNewFolder();
        }

        private void Shortcut(object sender, KeyEventArgs e)
        {
            HandleShortcut(sender, e);
        }

        private void BtnMove_Click(object sender, RoutedEventArgs e)
        {
            HandleBtnMove();
        }

        private void BtnCopy_Click(object sender, RoutedEventArgs e)
        {
            HandleBtnCopy();
        }

        private void Show_Checked(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ResetChecked(item);
            Reset();
            if (subMenuTab.IsChecked)
            {
                CreateExplorerTab();
            }
            ChangeOrientation();
        }

        private void TreeExplorer1_Collapsed(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = treeExplorer1.SelectedItem as TreeViewItem;
            item.Items.Clear();
        }

        private void TreeExplorer2_Collapsed(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = treeExplorer2.SelectedItem as TreeViewItem;
            item.Items.Clear();
        }

        private void BtnBack1_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            BackImageChange(btnBack1, imgBack1);
        }

        private void BtnBack2_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            BackImageChange(btnBack2, imgBack2);
        }

        private void SubMenuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void SubMenuCompare_Click(object sender, RoutedEventArgs e)
        {
            Methods.NotImplementedMessageBox("Compararea");
        }

        private void SubMenuUnpack_Click(object sender, RoutedEventArgs e)
        {
            HandleUnpack();
        }

        private void SubMenuPack_Click(object sender, RoutedEventArgs e)
        {
            HandlePack();
        }

        private void SubMenuAbout_Click(object sender, RoutedEventArgs e)
        {
            tabMain.Height = 20;
            tabAbout.Visibility = Visibility.Visible;
            tabAbout.Focus();
        }

        private void SubMenuLight_Click(object sender, RoutedEventArgs e)
        {
            Methods.Save("light");
            if (Message.RestartApplication() == MessageBoxResult.Yes)
            {
                Process.Start(System.Reflection.Assembly.GetEntryAssembly().Location);
                Application.Current.Shutdown();
            }
        }

        private void SubMenuDark_Click(object sender, RoutedEventArgs e)
        {
            Methods.Save("dark");
            if (Message.RestartApplication() == MessageBoxResult.Yes)
            {
                Process.Start(System.Reflection.Assembly.GetEntryAssembly().Location);
                Application.Current.Shutdown();
            }
        }

        private void SetTheme(ElementColor.Theme theme)
        {
            color = new ElementColor(theme);
            grdBase.Background = color.GetBackgroundColor();
            tabList.Background = color.GetBackgroundColor();
            tabMain.Background = color.GetMenuColor();
            tabAbout.Background = color.GetMenuColor();
            tabExtra.Background = color.GetMenuColor();
            grdMain.Background = color.GetBackgroundColor();
            treeExplorer1.Background = color.GetTreeColor();
            treeExplorer2.Background = color.GetTreeColor();

            btnBack1.Background = color.GetBackgroundColor();
            btnBack2.Background = color.GetBackgroundColor();
            btnCopy.Background = color.GetBackgroundColor();
            btnDelete.Background = color.GetBackgroundColor();
            btnMove.Background = color.GetBackgroundColor();
            btnNewFolder.Background = color.GetBackgroundColor();
            btnRefresh1.Background = color.GetBackgroundColor();
            btnRefresh2.Background = color.GetBackgroundColor();

            menuMain.Background = color.GetMenuColor();
            subMenuAbout.Background = color.GetMenuColor();
            subMenuCompare.Background = color.GetMenuColor();
            subMenuDark.Background = color.GetMenuColor();
            subMenuExit.Background = color.GetMenuColor();
            subMenuFull.Background = color.GetMenuColor();
            subMenuLight.Background = color.GetMenuColor();
            subMenuPack.Background = color.GetMenuColor();
            subMenuTab.Background = color.GetMenuColor();
            subMenuTree.Background = color.GetMenuColor();
            subMenuUnpack.Background = color.GetMenuColor();
            subMenuVertical.Background = color.GetMenuColor();

            menuMain.Foreground = color.GetTextColor();
            subMenuAbout.Foreground = color.GetTextColor();
            subMenuCompare.Foreground = color.GetTextColor();
            subMenuDark.Foreground = color.GetTextColor();
            subMenuExit.Foreground = color.GetTextColor();
            subMenuFull.Foreground = color.GetTextColor();
            subMenuLight.Foreground = color.GetTextColor();
            subMenuPack.Foreground = color.GetTextColor();
            subMenuTab.Foreground = color.GetTextColor();
            subMenuTree.Foreground = color.GetTextColor();
            subMenuUnpack.Foreground = color.GetTextColor();
            subMenuVertical.Foreground = color.GetTextColor();

            treeExplorer1.Foreground = color.GetTextColor();
            treeExplorer2.Foreground = color.GetTextColor();
            lblGroup.Foreground = color.GetTextColor();
            lblName.Foreground = color.GetTextColor();
            lblSurname.Foreground = color.GetTextColor();
            tabMain.Foreground = color.GetTextColor();
            tabExtra.Foreground = color.GetTextColor();
            tabAbout.Foreground = color.GetTextColor();

            switch (theme)
            {
                case ElementColor.Theme.Dark: imgBill.Source = new BitmapImage(new Uri("Icons/EasterEgg.png", UriKind.RelativeOrAbsolute)); break;
                case ElementColor.Theme.Light: imgBill.Source = new BitmapImage(new Uri("Icons/EasterEggLight.png", UriKind.RelativeOrAbsolute)); break;
            }
        }

        private void BtnCopy_IsHitTestVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (btnCopy.IsHitTestVisible)
            {
                imgCopy.Source = new BitmapImage(new Uri("Icons/Copy.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                imgCopy.Source = new BitmapImage(new Uri("Icons/CopyOff.png", UriKind.RelativeOrAbsolute));
            }
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink link = sender as Hyperlink;
            System.Diagnostics.Process.Start(link.NavigateUri.ToString());
        }

        private void BtnMove_IsHitTestVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (btnMove.IsHitTestVisible)
            {
                imgMove.Source = new BitmapImage(new Uri("Icons/Move.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                imgMove.Source = new BitmapImage(new Uri("Icons/MoveOff.png", UriKind.RelativeOrAbsolute));
            }
        }

        private void BtnDelete_IsHitTestVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (btnDelete.IsHitTestVisible)
            {
                imgDelete.Source = new BitmapImage(new Uri("Icons/Delete.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                imgDelete.Source = new BitmapImage(new Uri("Icons/DeleteOff.png", UriKind.RelativeOrAbsolute));
            }
        }

        private void BtnNewFolder_IsHitTestVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (btnNewFolder.IsHitTestVisible)
            {
                imgNewFolder.Source = new BitmapImage(new Uri("Icons/NewFolder.png", UriKind.RelativeOrAbsolute));
            }
            else
            {
                imgNewFolder.Source = new BitmapImage(new Uri("Icons/NewFolderOff.png", UriKind.RelativeOrAbsolute));
            }
        }

        
    }
}
