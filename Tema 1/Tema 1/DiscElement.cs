﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.Security.AccessControl;
using System.Security.Principal;
using System.IO.Compression;

namespace Tema_1
{
    public class DiscElement
    {
        public DiscElement(string file, Brush txtColor, bool isFull = true)
        {
            element = CreateElement(file, txtColor, isFull);
        }

        public DiscElement(TreeViewItem item)
        {
            element = item;
        }

        public DiscElement()
        {
            // empty
        }

        public TreeViewItem CreateElement(string file, Brush txtColor, bool isFull = true)
        {
            Grid grid = new Grid() { MinHeight = 20 };
            grid.CreateExplorerGrid();

            grid.AddItemToRow<TextBlock>(new TextBlock() { Text = file, Visibility = Visibility.Collapsed }, 0);
            grid.AddItemToRow<Image>(ImageSelector(file), 1);
            grid.AddItemToRow<TextBlock>(new TextBlock() { Text = Methods.GetFileName(file), Foreground = txtColor }, 2);

            if (isFull)
            {
                if (Directory.Exists(file))
                {
                    grid.AddItemToRow<TextBlock>(new TextBlock() { Text = Directory.GetCreationTime(file).ToString(), Foreground = txtColor }, 3);
                }
                if (File.Exists(file))
                {
                    grid.AddItemToRow<TextBlock>(new TextBlock() { Text = System.IO.File.GetCreationTime(file).ToString(), Foreground = txtColor }, 3);
                    grid.AddItemToRow<TextBlock>(new TextBlock() { Text = Methods.GetFileLength(file), Foreground = txtColor }, 4);
                }
            }

            if (isFull)
                return new TreeViewItem() { Header = grid, Margin = (Thickness)new ThicknessConverter().ConvertFromString("-20,0,0,0") };
            else
                return new TreeViewItem() { Header = grid, Margin = (Thickness)new ThicknessConverter().ConvertFromString("0,0,0,0") };
        }

        public string GetText()
        {
            Grid grid = element.Header as Grid;
            TextBlock text = grid.Children[0] as TextBlock;
            return text.Text;
        }

        public Image ImageSelector(string file)
        {
            string imgLocation;

            switch (Methods.GetFileType(file))
            {
                case "Drive": imgLocation = "Icons/Drive.png"; break;

                case "Folder": imgLocation = "Icons/Folder.png"; break;

                case "File": return Methods.RetrieveFileIcon(file);

                default: imgLocation = "Icons/File.png"; break;
            }

            return new Image()
            {
                Source = new BitmapImage(new Uri(imgLocation, UriKind.RelativeOrAbsolute)),
                Height = 15,
                Width = 15
            };
        }

        public TreeViewItem GetTreeViewItem()
        {
            return element;
        }

        private TreeViewItem element;
    }
}
