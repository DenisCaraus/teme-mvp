﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.Security.AccessControl;
using System.Security.Principal;

namespace Tema_1
{
    public static class TreeViewExtension
    {
        public static void PutDrivesInTree(this TreeView tree, Brush txtColor, bool isFull = true)
        {
            tree.Items.Clear();
            foreach (String drive in Directory.GetLogicalDrives())
            {
                DiscElement element = new DiscElement(drive, txtColor, isFull);
                tree.Items.Add(element.GetTreeViewItem());
            }
        }

        public static void Unselect(this TreeView tree)
        {
            if (tree.SelectedItem is TreeViewItem item)
                item.IsSelected = false;
        }

        public static bool FillTree(this TreeView tree, string directory, Brush txtColor, bool isFull = true)
        {
            if (directory == null)
            {
                tree.PutDrivesInTree(txtColor, isFull);
                return false;
            }
            if (isFull)
                return tree.AddElementsFull(directory, txtColor);
            else
                return tree.AddElementsTree(directory, txtColor);
        }

        private static bool AddElementsFull(this TreeView tree, string directory, Brush txtColor)
        {
            tree.Unselect();
            tree.Items.Clear();

            if (!RightsControl.CanAccess(directory))
            {
                MessageBoxResult result = MessageBox.Show("Acces neautorizat! ",
               "Eroare", MessageBoxButton.OK);
                tree.Back(directory, txtColor);
                return false;
            }

            try
            {
                foreach (String file in Directory.GetFileSystemEntries(directory))
                {
                    DiscElement element = new DiscElement(file, txtColor);
                    tree.Items.Add(element.GetTreeViewItem());
                }
            }
            catch(UnauthorizedAccessException e)
            {
                MessageBoxResult result = MessageBox.Show("Acces neautorizat! ",
                "Eroare", MessageBoxButton.OK);
                tree.Back(directory, txtColor);
                return false;
            }

            return true;
        }

        // find better way to refresh
        private static bool AddElementsTree(this TreeView tree, string directory, Brush txtColor)
        {
            TreeViewItem item = tree.Find(directory);
            item.Items.Clear();
            try
            {
                foreach (String file in Directory.GetFileSystemEntries(directory))
                {
                    DiscElement element = new DiscElement(file, txtColor, false);
                    tree.Items.Add(element.GetTreeViewItem());
                }
            }
            catch (UnauthorizedAccessException e)
            {
                MessageBoxResult result = MessageBox.Show("Acces neautorizat! ",
               "Eroare", MessageBoxButton.OK);
                return false;
            }
            return true;
        }

        public static string Back(this TreeView tree, string directory, Brush txtColor)
        {
            System.IO.DirectoryInfo parent = Directory.GetParent(directory);

            if (parent == null)
            {
                tree.PutDrivesInTree(txtColor);
                return null;
            }

            tree.FillTree(parent.ToString(), txtColor);

            return parent.ToString();
        }

        public static void Generate(this TreeView tree, string parent, Brush txtColor)
        {
            if (parent == null)
                tree.PutDrivesInTree(txtColor);
            else
                tree.FillTree(parent, txtColor);
        }

        public static TreeViewItem Find(this TreeView tree, string directory)
        {
            foreach (TreeViewItem item in tree.Items)
            {
                Grid grid = item.Header as Grid;
                if ((grid.Children[0] as TextBlock).Text.Equals(directory))
                    return item;
                TreeViewItem tmpItem = item.Find(directory);
                if (tmpItem != null)
                    return tmpItem;
            }
            return null;
        }
    }
}
