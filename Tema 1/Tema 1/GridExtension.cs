﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Diagnostics;
using System.Windows.Interop;
using System.Security.AccessControl;
using System.Security.Principal;
namespace Tema_1
{
    public static class GridExtension
    {
        public static void CreateExplorerGrid(this Grid grid)
        {
            List<ColumnDefinition> columns = new List<ColumnDefinition>
            {
                new ColumnDefinition(),
                new ColumnDefinition() { MinWidth = 30 },
                new ColumnDefinition() { MinWidth = 430 },
                new ColumnDefinition() { MinWidth = 130 },
                new ColumnDefinition()
            };

            foreach (ColumnDefinition column in columns)
            {
                grid.ColumnDefinitions.Add(column);
            }
        }

        public static void AddItemToRow<T>(this Grid grid, T item, int column) where T : System.Windows.UIElement
        {
            grid.Children.Add(item);
            Grid.SetColumn(item, column);
        }

        public static void CreateTabGrid(this Grid grid, Button btn1, Button btn2, TreeView tree)
        {
            RowDefinition def0 = new RowDefinition { Height = (GridLength)new GridLengthConverter().ConvertFromString("40") };
            RowDefinition def1 = new RowDefinition { Height = (GridLength)new GridLengthConverter().ConvertFromString("*") };

            grid.RowDefinitions.Add(def0);
            grid.RowDefinitions.Add(def1);

            grid.Children.Add(btn1);
            Grid.SetRow(btn1, 0);
            grid.Children.Add(btn2);
            Grid.SetRow(btn2, 0);
            grid.Children.Add(tree);
            Grid.SetRow(tree, 1);
        }
    }
}
