﻿using System.Windows.Media;

namespace Tema_1
{
    public class ElementColor
    {
        public enum Theme
        {
            Light,
            Dark
        }

        private Brush menu, background, backgroundTree, text, borderColor;

        public ElementColor(Theme theme = Theme.Light)
        {
            SetTheme(theme);
        }

        public Brush GetBorderColor()
        {
            return borderColor;
        }

        public Brush GetMenuColor()
        {
            return menu;
        }

        public Brush GetBackgroundColor()
        {
            return background;
        }

        public Brush GetTreeColor()
        {
            return backgroundTree;
        }

        public Brush GetTextColor()
        {
            return text;
        }

        public void SetTheme(Theme theme)
        {
            switch(theme)
            {
                case Theme.Light: SetLightTheme(); break;
                case Theme.Dark: SetDarkTheme(); break;
            }
        }

        private void SetLightTheme()
        {
            background = Brushes.GhostWhite;
            backgroundTree = Brushes.White;
            text = Brushes.Black;
            menu = Brushes.WhiteSmoke;
            borderColor = Brushes.Black;
        }

        private void SetDarkTheme()
        {
            background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF1F1F1F"));
            backgroundTree = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF2C2C2C"));
            text = Brushes.LightGray;
            menu = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF2C2C2C"));
            borderColor = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF2C2C2C"));
        }
    }
}
