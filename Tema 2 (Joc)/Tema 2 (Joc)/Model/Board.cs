﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2__Joc_.Model
{
    public class Board
    {
        private const char emptySymbol = ' ';
        private char player1Symbol;
        private char player2Symbol;
        private int boardSize;
        private List<List<Cell<char, string>>> grid;

        public Board(int boardSize, char player1Symbol = 'X', char player2Symbol = 'O')
        {
            this.boardSize = boardSize;
            this.player1Symbol = player1Symbol;
            this.player2Symbol = player2Symbol;
            SetGrid(InitializeBoard(), InitializePositionList());
        }

        public Board(string file)
        {
            LoadFile(file);
        }

        private void LoadFile(string file)
        {
            string[] lines = file.Split('\n');
            bool isFirstLine = true;
            List<List<char>> grid = new List<List<char>>();

            foreach (string line in lines)
            {
                string[] temp = line.Split(',');
                if (isFirstLine)
                {              
                    this.player1Symbol = temp[0][0];
                    this.player2Symbol = temp[1][0];
                    this.boardSize = int.Parse(temp[2]);
                    isFirstLine = false;
                    continue;
                }

                if(line.Length < boardSize * 2)
                {
                    break;
                }

                List<char> tempColumn = new List<char>();
                for (int column = 0; column < boardSize; ++column)
                {
                    char symbol = temp[column][0];
                    if (symbol == '?')
                    {
                        symbol = emptySymbol;
                    }
                    tempColumn.Add(symbol);
                }
                grid.Add(tempColumn);
            }
            SetGrid(grid, InitializePositionList());
        }

        private List<List<char>> InitializeBoard()
        {
            List<List<char>> grid = new List<List<char>>();
            for (int index1 = 0; index1 < boardSize; ++index1)
            {
                List<char> temp = new List<char>();
                for(int index2 = 0; index2 < boardSize; ++index2)
                {
                    temp.Add(emptySymbol);
                }
                grid.Add(temp);
            }

            return grid;
        }

        private List<List<string>> InitializePositionList()
        {
            List<List<string>> positionGrid = new List<List<string>>();
            for (int index1 = 0; index1 < boardSize; ++index1)
            {
                List<string> temp = new List<string>();
                for (int index2 = 0; index2 < boardSize; ++index2)
                {
                    temp.Add(index1.ToString() + "," + index2.ToString());
                }
                positionGrid.Add(temp);
            }

            return positionGrid;
        }

        public void SetGrid(List<List<char>> grid, List<List<string>> positionGrid)
        {
            this.grid = new List<List<Cell<char, string>>>();
            for (int index1 = 0; index1 < grid.Count(); ++index1)
            {
                var temp = new List<Cell<char, string>>();
                for (int index2 = 0; index2 < grid.Count(); ++index2)
                {
                    temp.Add(new Cell<char, string>(grid[index1][index2], positionGrid[index1][index2]));
                }
                this.grid.Add(temp);
            }
        }

        public char Player1Symbol
        {
            get
            {
                return player1Symbol;
            }
        }

        public char Player2Symbol
        {
            get
            {
                return player2Symbol;
            }
        }

        public int BoardSize
        {
            get
            {
                return boardSize;
            }
        }

        public List<List<Cell<char, string>>> Grid
        {
            get
            {
                return grid;
            }

        }


        public bool SetTile(int rowPosition, int colPosition, int player)
        {
            if((rowPosition >= boardSize) && (colPosition >= boardSize))
            {
                return false;
            }

            char playerCharacter = GetPlayerCharacter(player);

            if(grid[rowPosition][colPosition].Item1 == playerCharacter)
            {
                return false;
            }
            grid[rowPosition][colPosition].Item1 = playerCharacter;
            return true;
        }

        public char GetPlayerCharacter(int playerNumber)
        {
            switch (playerNumber)
            {
                case 1: return player1Symbol;
                case 2: return player2Symbol;
                default: return emptySymbol;
            }
        }

        public override string ToString()
        {
            string temp = String.Empty;

            temp += player1Symbol + "," + player2Symbol + "," + boardSize.ToString() + "\n";

            for(int row = 0; row < boardSize; ++row)
            {
                for(int column = 0; column < boardSize; ++column)
                {
                    if(grid[row][column].Item1 == emptySymbol)
                    {
                        temp += "?" + ",";
                        continue;
                    }
                    temp += grid[row][column].Item1 + ",";
                }
                temp += "\n";
            }

            return temp;
        }
    }
}
