﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2__Joc_.Model
{
    public class Game
    {
        private Player player1;
        private Player player2;
        private bool isFirstPlayerTurn;
        private Board board;
        private bool isGameOver;
        private string boardSave;

        public Game(Player player1, Player player2, int boardSize)
        {
            this.player1 = player1;
            this.player2 = player2;
            board = new Board(boardSize);
            isFirstPlayerTurn = true;
            isGameOver = false;
        }

        public Game(Player player1, Player player2, string board, bool isFirstPlayerTurn, bool isGameOver)
        {
            this.player1 = player1;
            this.player2 = player2;
            this.board = new Board(board);
            this.isFirstPlayerTurn = isFirstPlayerTurn;
            this.isGameOver = isGameOver;
        }

        public Player CurrentPlayer
        {
            get
            {
                if(isFirstPlayerTurn)
                {
                    return player1;
                }
                return player2;
            }
        }

        public Player Player1
        {
            get
            {
                return player1;
            }
        }

        public Player Player2
        {
            get
            {
                return player2;
            }
        }

        public int BoardSize
        {
            get
            {
                return board.BoardSize;
            }
        }

        public Board Board
        {
            get
            {
                return board;
            }
        }

        public string GameStatus
        {
            get
            {
                string temp = String.Empty;
                switch (isGameOver)
                {
                    case false: temp += "Current player: "; break;
                    case true: temp += "Winner: "; break;
                }
                switch (isFirstPlayerTurn)
                {
                    case true: temp += player1.Name; break;
                    case false: temp += player2.Name; break;
                }
                return temp;
            }
        }

        public bool IsGameOver
        {
            get
            {
                return isGameOver;
            }
        }

        public bool Next(int row, int column)
        {
            if (!IsValid(row, column))
            {
                return false;
            }

            board.SetTile(row, column, SelectCurrentPlayer());

            if (VictoryCheck())
            {
                isGameOver = true;
                return true;
            }

            isFirstPlayerTurn = !isFirstPlayerTurn;

            return true;
        }

        private int SelectCurrentPlayer()
        {
            if (isFirstPlayerTurn)
            {
                return 1;
            }
            return 2;
        }

        private bool IsValid(int row, int column)
        {
            if (isGameOver)
            {
                return false;
            }
            if (board.Grid[row][column].Item1 != ' ')
            {
                return false;
            }
            return true;
        }

        private bool VictoryCheck()
        {
            char playerCharacter = board.GetPlayerCharacter(SelectCurrentPlayer());

            if (MainDiagonalCheck(playerCharacter))
            {
                return true;
            }
            if (SecondaryDiagonalCheck(playerCharacter))
            {
                return true;
            }
            if (RowCheck(playerCharacter))
            {
                return true;
            }
            if (ColumnCheck(playerCharacter))
            {
                return true;
            }

            return false;
        }

        private bool RowCheck(char playerCharacter)
        {
            bool win;
            for (int row = 0; row < BoardSize; ++row)
            {
                win = true;
                for (int column = 0; column < BoardSize; ++column)
                {
                    if (board.Grid[row][column].Item1 != playerCharacter)
                    {
                        win = false;
                        break;
                    }
                }
                if (win)
                    return true;
            }
            return false;
        }

        private bool ColumnCheck(char playerCharacter)
        {
            bool win;
            for (int column = 0; column < BoardSize; ++column)
            {
                win = true;
                for (int row = 0; row < BoardSize; ++row)
                {
                    if (board.Grid[row][column].Item1 != playerCharacter)
                    {
                        win = false;
                        break;
                    }
                }
                if (win)
                    return true;
            }
            return false;
        }

        private bool MainDiagonalCheck(char playerCharacter)
        {
            int row = 0, column = 0;
            while ((row < BoardSize) && (column < BoardSize))
            {
                if (board.Grid[row][column].Item1 != playerCharacter)
                {
                    return false;
                }
                row++;
                column++;
            }
            return true;
        }

        private bool SecondaryDiagonalCheck(char playerCharacter)
        {
            int row = 0, column = BoardSize - 1;
            while ((row < BoardSize) && (column >= 0))
            {
                if (board.Grid[row][column].Item1 != playerCharacter)
                {
                    return false;
                }
                row++;
                column--;
            }
            return true;
        }

        public override string ToString()
        {
            string temp = String.Empty;
            temp += player1.Name + "," + player2.Name + "," + isFirstPlayerTurn.ToString() + ","  + isGameOver.ToString();
            return temp;
        }
    }
}
