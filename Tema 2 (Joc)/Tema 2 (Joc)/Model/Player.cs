﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2__Joc_.Model
{
    public class Player
    {
        private string name, image;
        private int wonGames, playedGames;
        private bool isActive;

        // de modificat
        public Player(string name, string image)
        {
            this.name = name;
            this.image = image;
            this.wonGames = 0;
            this.playedGames = 0;
            isActive = true;
        }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public string Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }

        public int WonGames
        {
            get
            {
                return wonGames;
            }
            set
            {
                wonGames = value;
            }
        }

        public int PlayedGames
        {
            get
            {
                return playedGames;
            }
            set
            {
                playedGames = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }
            set
            {
                isActive = value;
            }
        }
        
        public void Delete()
        {
            isActive = false;
        }

        override public string ToString()
        {
            return name + " " + image + " " + wonGames.ToString() + " " + playedGames.ToString() + " " + isActive.ToString();
        }
    }
}
