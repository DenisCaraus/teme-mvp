﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_2__Joc_.Model
{
    public class LoadedGame
    {
        private readonly string player1, player2, boardSave;
        private readonly bool isGameOver, isPlayer1Turn;

        public LoadedGame(string player1, string player2, bool isPlayer1Turn, bool isGameOver, string boardSave)
        {
            this.player1 = player1;
            this.player2 = player2;
            this.isPlayer1Turn = isPlayer1Turn;
            this.isGameOver = isGameOver;
            this.boardSave = boardSave;   
        }

        public string Player1
        {
            get
            {
                return player1;
            }
        }

        public string Player2
        {
            get
            {
                return player2;
            }
        }

        public bool IsPlayer1Turn
        {
            get
            {
                return isPlayer1Turn;
            }
        }

        public bool IsGameOver
        {
            get
            {
                return isGameOver;
            }
        }

        public string BoardSave
        {
            get
            {
                return boardSave;
            }
        }
    }
}
