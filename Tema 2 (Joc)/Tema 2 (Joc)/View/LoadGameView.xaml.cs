﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tema_2__Joc_.ViewModel;

namespace Tema_2__Joc_.View
{
    /// <summary>
    /// Interaction logic for LoadView.xaml
    /// </summary>
    public partial class LoadGameView : Window
    {
        public LoadGameView()
        {
            InitializeComponent();
        }

        public LoadGameView(LoadGameViewModel context) : this()
        {
            this.DataContext = context;
        }

        void Exit(object sender, EventArgs e)
        {
            this.Close();
        }

        void Cancel(object sender, EventArgs e)
        {
            ((LoadGameViewModel)this.DataContext).Reset();
            this.Close();
        }
    }
}
