﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tema_2__Joc_.ViewModel;

namespace Tema_2__Joc_.View
{
    /// <summary>
    /// Interaction logic for SignInView.xaml
    /// </summary>
    public partial class SignInView : UserControl
    {
        public SignInView()
        {
            InitializeComponent();
            this.DataContext = new Tema_2__Joc_.ViewModel.SignInViewModel();
        }

        private void BtnNew_Click(object sender, RoutedEventArgs e)
        {
            var customerDialogBox = new NewPlayerDialog(((SignInViewModel)this.DataContext).newPlayer);
            customerDialogBox.ShowDialog();
            ((SignInViewModel)this.DataContext).AddPlayer();
        }

        private void BtnPlay_Click(object sender, RoutedEventArgs e)
        {
            var game = new GameView(new GameViewModel(((SignInViewModel)this.DataContext).FirstPlayer, ((SignInViewModel)this.DataContext).SecondPlayer, ((SignInViewModel)this.DataContext).BoardSize));
            game.ShowDialog();
            ((SignInViewModel)this.DataContext).UpdateStats(((GameViewModel)game.DataContext).CurrentPlayer, ((GameViewModel)game.DataContext).IsGameOver);
            BtnExit_Click(null, null);
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            ((SignInViewModel)this.DataContext).SavePlayers();
            System.Windows.Application.Current.Shutdown();
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            ((SignInViewModel)this.DataContext).OnNext();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure?", "Warning", MessageBoxButton.OKCancel);
            ((SignInViewModel)this.DataContext).DeleteSelectedPlayer(result == MessageBoxResult.OK);
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            var customerDialogBox = new LoadGameView(((SignInViewModel)this.DataContext).loadGames);
            customerDialogBox.ShowDialog();

            if (((SignInViewModel)this.DataContext).StartLoadedGame())
            {
                var game = new GameView(new GameViewModel(((SignInViewModel)this.DataContext).FirstPlayer, ((SignInViewModel)this.DataContext).SecondPlayer, ((SignInViewModel)this.DataContext).loadGames.SelectedGame.BoardSave, ((SignInViewModel)this.DataContext).loadGames.SelectedGame.IsPlayer1Turn, ((SignInViewModel)this.DataContext).loadGames.SelectedGame.IsGameOver));
                game.ShowDialog();
                ((SignInViewModel)this.DataContext).UpdateStats(((GameViewModel)game.DataContext).CurrentPlayer, ((GameViewModel)game.DataContext).IsGameOver);
                BtnExit_Click(null, null);
            }
        }
    }
}
