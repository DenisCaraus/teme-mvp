﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tema_2__Joc_.ViewModel;

namespace Tema_2__Joc_.View
{
    /// <summary>
    /// Interaction logic for GameView.xaml
    /// </summary>
    public partial class GameView : Window
    {

        public GameView()
        {
            InitializeComponent();
        }

        public GameView(GameViewModel context) : this()
        {
            this.DataContext = context;
            this.MaxWidth = ((GameViewModel)this.DataContext).BoardSize * 70;
            this.Height = ((GameViewModel)this.DataContext).BoardSize * 70 + 60; 
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {     
            ((GameViewModel)this.DataContext).OnAction(((Button)sender).Tag.ToString());
            var data = this.DataContext;
            this.DataContext = null;
            this.DataContext = data;
        }

        private void SubMenuSave_Click(object sender, RoutedEventArgs e)
        {
            ((GameViewModel)this.DataContext).Save();
        }
    }
}
