﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Tema_2__Joc_.Model;

namespace Tema_2__Joc_.ViewModel
{
    public class GameViewModel : ViewModelBase
    {
        private Game game;

        public GameViewModel(Player player1, Player player2, int boardSize)
        {
            this.game = new Game(player1, player2, boardSize);
        }

        public GameViewModel(Player player1, Player player2, string savedBoard, bool isFirstPlayerTurn, bool isGameOver)
        {
            savedBoard = Path.GetFullPath("..\\..\\Resources\\SavedGames\\Boards\\" + savedBoard);
            savedBoard = File.ReadAllText(savedBoard);

            this.game = new Game(player1, player2, savedBoard, isFirstPlayerTurn, isGameOver);

            RaisePropertyChangedEvent(String.Empty);
        }

        public bool IsGameOver
        {
            get
            {
                return game.IsGameOver;
            }
        }

        public Player CurrentPlayer
        {
            get
            {
                return game.CurrentPlayer;
            }
        }

        public Player Player1
        {
            get
            {
                return game.Player1;
            }
        }

        public Player Player2
        {
            get
            {
                return game.Player2;
            }
        }

        public int BoardSize
        {
            get
            {
                return game.BoardSize;
            }
        }

        public List<List<Cell<char, string>>> Board
        {
            get
            {
                return game.Board.Grid;
            }

        }

        public string GameStatus
        {
            get
            {
                return game.GameStatus;
            }
        }

        public void OnAction(string param)
        {
            if (param == "")
                return;

            int row = int.Parse(param.Split(',')[0]);
            int column = int.Parse(param.Split(',')[1]);

            game.Next(row, column);
        }

        public void Save()
        {
            string boardFile = DateTime.Now.ToString("[dd mm yyyy][HH mm ss]") + ".txt";
            string lines = game.ToString() + "," + boardFile + "\n";

            File.AppendAllText(Path.GetFullPath("..\\..\\Resources\\SavedGames\\Games.txt"), lines);
            File.AppendAllText(Path.GetFullPath("..\\..\\Resources\\SavedGames\\Boards\\" + boardFile), game.Board.ToString());
        }
    }
}
