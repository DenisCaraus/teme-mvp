﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tema_2__Joc_.Model;


// modify to remove and shet
namespace Tema_2__Joc_.ViewModel
{
    public class LoadGameViewModel : ViewModelBase
    {
        private ObservableCollection<LoadedGame> games;
        private LoadedGame selectedGame;
        private bool canLoad;

        public LoadGameViewModel()
        {
            LoadGames();
            canLoad = false;
        }

        public ObservableCollection<LoadedGame> Games
        {
            get
            {
                return games;
            }
        }

        public LoadedGame SelectedGame
        {
            get
            {
                return selectedGame;
            }
            set
            {
                selectedGame = value;
                if (value != null)
                {
                    canLoad = true;
                }
                RaisePropertyChangedEvent(String.Empty);
            }
        }

        public bool CanLoad
        {
            get
            {
                return canLoad;
            }
        }

        private void LoadGames()
        {
            ObservableCollection<LoadedGame> games = new ObservableCollection<LoadedGame>();
            string[] lines = File.ReadAllLines(Path.GetFullPath("..\\..\\Resources\\SavedGames\\Games.txt"));

            foreach (string line in lines)
            {
                string[] game = line.Split(',');

                if (game.Count() != 5) { return; }

                games.Add(new LoadedGame(game[0], game[1], bool.Parse(game[2]), bool.Parse(game[3]), game[4]));
            }

            this.games = games;
        }

        public void Reset()
        {
            canLoad = false;
        }
    }
}
