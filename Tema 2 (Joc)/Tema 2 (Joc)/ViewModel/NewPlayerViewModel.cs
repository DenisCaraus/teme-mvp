﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tema_2__Joc_.Model;

namespace Tema_2__Joc_.ViewModel
{
    public class NewPlayerViewModel : ViewModelBase
    {
        private string name, selectedItem;
        private bool isValid;
        private ObservableCollection<string> images;

        public NewPlayerViewModel()
        {
            LoadImages();
            isValid = false;
        }

        public bool IsValid
        {
            get
            {
                return isValid;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                isValid = true;
                RaisePropertyChangedEvent(String.Empty);
            }
        }

        public string SelectedItem
        {
            get
            {
                return "..\\..\\Resources\\Images\\" + selectedItem + ".bmp";
            }
            set
            {
                selectedItem = value;
                RaisePropertyChangedEvent(String.Empty);
            }
        }

        public string Image
        {
            get
            {
                return Path.GetFullPath(SelectedItem);
            }
        }

        public ObservableCollection<string> Images
        {
            get
            {
                return images;
            }
        }

        private void LoadImages()
        {
            images = new ObservableCollection<string>();

            foreach (string file in Directory.GetFiles(Path.GetFullPath("..\\..\\Resources\\Images")))
            {
                images.Add(Path.GetFileNameWithoutExtension(file));
            }

            this.selectedItem = images[0];
        }

        public void Reset()
        {
            name = "";
            selectedItem = images[0];
            isValid = false;
        }
    }
}
