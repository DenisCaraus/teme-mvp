﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tema_2__Joc_.Model;
using System.Collections.ObjectModel;
using System.IO;
using System.ComponentModel;

namespace Tema_2__Joc_.ViewModel
{
    public class SignInViewModel : ViewModelBase
    {
        private Player firstPlayer, secondPlayer, selectedPlayer;
        private bool canDelete, canSelect, canStart;
        private string error, info, boardSizeString;
        private int boardSize;
        private ObservableCollection<Player> players;

        public NewPlayerViewModel newPlayer;
        public LoadGameViewModel loadGames;

        public SignInViewModel()
        {
            LoadPlayers();
            this.newPlayer = new NewPlayerViewModel();
            this.loadGames = new LoadGameViewModel();
            canSelect = true;
            canDelete = false;
            canStart = false;
            info = "Select a player and press the 'Next' button!";
        }

        

        public ObservableCollection<Player> Players
        {
            get
            {
                return players;
            }
        }

        public string Info
        {
            get
            {
                return info;
            }
        }

        public Player SelectedPlayer
        {
            get
            {
                return selectedPlayer;
            }
            set
            {
                selectedPlayer = value;
                if (value == null)
                {
                    canDelete = false;
                }
                if (value != null)
                {
                    canDelete = (firstPlayer == null);
                }
                RaisePropertyChangedEvent(String.Empty);
            }
        }

        public string Image
        {
            get
            {
                if(SelectedPlayer == null)
                {
                    return Path.GetFullPath("..\\..\\Resources\\Images\\default.bmp");
                }
                return Path.GetFullPath(SelectedPlayer.Image);
            }
        }

        public Player FirstPlayer
        {
            get
            {
                return firstPlayer;
            }
        }

        public Player SecondPlayer
        {
            get
            {
                return secondPlayer;
            }
        }

        public bool CanSelect
        {
            get
            {
                return canSelect;
            }
        }

        public bool CanStart
        {
            get
            {
                return canStart;
            }
        }

        public bool CanDelete
        {
            get
            {
                return canDelete;
            }
        }

        public bool CanLoad
        {
            get
            {
                return File.Exists(Path.GetFullPath("..\\..\\Resources\\SavedGames\\Games.txt"));
            }
        }

        public string ErrorMessage
        {
            get
            {
                return error;
            }
        }

        public string BoardSizeString
        {
            get
            {
                return boardSizeString;
            }
            set
            {
                boardSizeString = value;
            }
        }

        public int BoardSize
        {
            get
            {
                return boardSize;
            }
        }

        public void LoadPlayers()
        {
            ObservableCollection<Player> players = new ObservableCollection<Player>();
            string[] lines = File.ReadAllLines(Path.GetFullPath("..\\..\\Resources\\SavedPlayers\\Players.txt"));

            foreach (string line in lines)
            {
                string[] player = line.Split(' ');

                if (player.Count() != 5) { return; }

                players.Add(new Player(player[0], player[1]) { WonGames = int.Parse(player[2]), PlayedGames = int.Parse(player[3]), IsActive = bool.Parse(player[4]) });
            }

            this.players = players;
        }

        public void SavePlayers()
        {
            string lines = String.Empty;

            foreach (Player player in players)
            {
                lines += player.ToString() + "\n";
            }

            File.WriteAllText(Path.GetFullPath("..\\..\\Resources\\SavedPlayers\\Players.txt"), lines);
        }

        public void AddPlayer()
        {
            if(!this.newPlayer.IsValid)
            {
                this.newPlayer.Reset();
                return;
            }

            int playerPosition = FindPlayerPosition(this.newPlayer.Name);

            if (playerPosition != -1)
            {
                players[playerPosition].Image = this.newPlayer.SelectedItem;
                this.newPlayer.Reset();
                return;
            }

            Players.Add(new Player(this.newPlayer.Name, this.newPlayer.SelectedItem));
            this.newPlayer.Reset();
        }

        public bool StartLoadedGame()
        {
            if (!loadGames.CanLoad)
            {
                return false;
            }

            LoadedGame loadGame = loadGames.SelectedGame;
            int positionPlayer1 = FindPlayerPosition(loadGame.Player1);
            int positionPlayer2 = FindPlayerPosition(loadGame.Player2);

            if((positionPlayer1 == -1)&& (positionPlayer2 == -1))
            {
                return false;
            }

            firstPlayer = players[positionPlayer1];
            secondPlayer = players[positionPlayer2];

            return true;
        }

        public void OnNext()
        {
            error = String.Empty;

            if ((SelectedPlayer == null) && (secondPlayer == null))
            {
                error = "You need to select a player";
                RaisePropertyChangedEvent(String.Empty);
                return;
            }

            if (firstPlayer == null)
            {
                canDelete = false;
                firstPlayer = SelectedPlayer;
                info = "Select another player and press the 'Next' button!";
                RaisePropertyChangedEvent(String.Empty);
                return;
            }

            if(selectedPlayer == firstPlayer)
            {
                error = "The second player needs to be different from the first";
                RaisePropertyChangedEvent(String.Empty);
                return;
            }

            if (secondPlayer == null)
            {
                secondPlayer = SelectedPlayer;
                info = "Insert the board size and press the 'Next' button!";
                RaisePropertyChangedEvent(String.Empty);
                return;
            }

            if (int.TryParse(boardSizeString, out int number))
            {
                boardSize = number;
                canSelect = false;
                canStart = true;
                info = "To start the game press the 'Start' button!";
                RaisePropertyChangedEvent(String.Empty);
                return;
            }

            error = "Incorrect board size! ( Tip: You should introduce a number in the box )";
            RaisePropertyChangedEvent(String.Empty);
        }

        public void DeleteSelectedPlayer(bool delete)
        {
            if (delete)
            {
                Player backup = selectedPlayer;
                selectedPlayer.Delete();
                players.Remove(selectedPlayer);
                players.Add(backup);
            }
        }
        
        public void UpdateStats(Player currentPlayer, bool isGameOver)
        {
            if(!isGameOver)
            {
                return;
            }

            if(currentPlayer == firstPlayer)
            {
                firstPlayer.WonGames++;
            }
            if (currentPlayer == secondPlayer)
            {
                secondPlayer.WonGames++;
            }

            firstPlayer.PlayedGames++;
            secondPlayer.PlayedGames++;

            SavePlayers();
        }

        private int FindPlayerPosition(string playerName)
        {
            int count = 0;
            foreach (Player player in players)
            {
                if (player.Name == playerName)
                {
                    return count;
                }
                count++;
            }
            return -1;
        }
    }
}
